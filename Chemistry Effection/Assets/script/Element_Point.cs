﻿using UnityEngine;
using System.Collections;

public class Element_Point : MonoBehaviour {
	public GameObject element;
	private Collider otherCollider = null;
	public bool element_point = false;
	public GameObject player;
	// Use this for initialization

	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(player.GetComponent<Player>().IsTrigger())
		{
			Instantiate (element, gameObject.transform.position, gameObject.transform.rotation);
			element_point = false;
		}
		
	}
	void OnTriggerEnter(Collider other)
	{
		otherCollider = other;
	}

	void OnTriggerExit(Collider other)
	{
		otherCollider = null;
	}
}
