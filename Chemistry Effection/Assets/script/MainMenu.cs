﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

	public GameObject PauseUI;
	//private bool paused = false;



	void Start () 
	{
	}
	

	void Update () 
	{
	}

	public void Restart()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}
	public void GameScene()
	{
		SceneManager.LoadScene("Game");
	}

	public void Quit()
	{
		Application.Quit ();
	}
}
