﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour {

	public GameObject PauseUI;
	public GameObject TabUI;
	private bool paused = false;
	private bool Tabed = false;


	void Start () 
	{
		PauseUI.SetActive (false);
	}
	

	void Update () 
	{
		if (Input.GetKeyDown( KeyCode.Tab))
		{
			Tabed = !Tabed;
		}
		if(Tabed)
		{
			TabUI.SetActive(true);
		}
		if(!Tabed)
		{
			TabUI.SetActive(false);
		}
		if (Input.GetButtonDown ("pause")) 
		{
			paused = !paused;
		}
		if (paused) 
		{
			PauseUI.SetActive (true);
			Time.timeScale = 0;
		}
		if (!paused) 
		{
			PauseUI.SetActive (false);
			Time.timeScale = 1;
		}
	}


	public void Resume()
	{
		paused = false;
	}


	public void Restart()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}


	public void MainMenu()
	{
		SceneManager.LoadScene("Mine");
	}

	public void Quit()
	{
		Application.Quit ();
	}
}
