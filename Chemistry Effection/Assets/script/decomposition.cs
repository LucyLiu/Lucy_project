﻿using UnityEngine;
using System.Collections;

public class decomposition : MonoBehaviour {
	public bool isCollide = false;
	public GameObject EUI;
	public string labelText;
	private float initTime = 0;
	public bool isTrigger = false;	
	//public Transform OxygenPoint;
	//public GameObject Oxygen;
	// Use this for initialization
	void Start () {
	
	}

	void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.tag == "Player") 
		{
			labelText = "Press E to pick up pistol.";
			isCollide = true;
		}
	}
	void OnTriggerExit(Collider col)
	{
		isCollide = false;
	}
	// Update is called once per frame
	void Update () {
	
		if(isTrigger){
			//gameObject.GetComponent<Element_Point>().element_point = true;
			if(initTime >= 3)
			{
				isTrigger = false;
				initTime = 0;
				EUI.SetActive(false);
				//Instantiate (Oxygen, OxygenPoint.position, gameObject.transform.rotation);
			}
			else
			initTime += Time.deltaTime;
		}

	}
	void OnGUI()
	{
		if (isCollide) 
		{
			GUI.Box (new Rect (Screen.width/2-100, Screen.height - 50, 200, 25), (labelText));
		}
	}
}
